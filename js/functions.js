var width  = 1200,
    height = 600;

var strokeWidth = 2;

var xScale = d3.scaleLinear()
    .domain([0-100, 6800+100])
    .range([0, width]);

var yScale = d3.scaleLinear()
    .domain([0-200, 6200+200])
    .range([0, height]);

var labelSize = 9;

var customHash = function(str) {
    var hash = 0;
    var a = 63689;
    var b = 378551;
    for(var i = 0; i < str.length; i++) {
        hash = (hash * a + str.charCodeAt(i)) % 359;
        a = (a * b) % 359;
    }
    return hash;
};

var colorHash = new ColorHash({hash: customHash, saturation: 0.75, lightness: .5});
var darkColorHash = new ColorHash({hash: customHash, saturation: 0.75, lightness: .3});

function processData(){
  cities.sort(function(a,b){return b.expense_value - a.expense_value});
  
  for (var i = 0; i < tribes.length; i++) {
    var player = players.find(x => x.id == tribes[i].owner_player_id);
    tribes[i].tribe_id = player.tribe_id;
  }
  
  for (var i = 0; i < cities.length; i++) {
    var player = players.find(x => x.id == cities[i].player_id);
    cities[i].player = player.name;
    var tribe_id = player.tribe_id;
    cities[i].tribe_id = tribe_id;
    var tribe = tribes.find(x => x.tribe_id == tribe_id);
    if (tribe == null){
      cities[i].tribe = null;
    }
    else{
      cities[i].tribe = tribe.name;
    }
    cities[i].ind = i;
    
    cities[i].tooltip = cities[i].name + '<div class="subtitle">' + cities[i].player + '<br />' + (cities[i].tribe == null? "n/a": cities[i].tribe)
      +'<br />' + cities[i].x + ',' + cities[i].y + '</div>';
  }
  
  for (var i = 0; i < cities.length; i++) {
    cities[i].color = colorHash.hex(cities[i].player);
    cities[i].textColor = darkColorHash.hex(cities[i].player);
  }
  
  for (var i = 0; i < strongholds.length; i++){
    if (strongholds[i].owner_tribe_id != null){
      var tribe = tribes.find(x => x.tribe_id == strongholds[i].owner_tribe_id);
      strongholds[i].tribe = tribe.name;
    }
    
  }
  
  projectCoords(cities);
  projectCoords(forests);
  projectCoords(barbs);
  projectCoords(strongholds);
}

function setColor(elem, attr){
  data = eval(elem);
  if (attr == 'none') {
    for (var i = 0; i < data.length; i++) {
      data[i].color = "#444"
      data[i].textColor = "#000";
    }
  }
  else if (data.length > 0) {
    if (typeof(data[0][attr]) == "number"){
      var minVal = Math.min.apply(Math, data.map(function(o) { return o[attr]; }));
      var maxVal = Math.max.apply(Math, data.map(function(o) { return o[attr]; }));
      var color = d3.scaleSequential(d3.interpolateViridis)
        .domain([maxVal, minVal]);
      for (var i = 0; i < data.length; i++) {
        data[i].color = color(data[i][attr]);
        data[i].textColor = color(data[i][attr]);
      }
    }
    else{
      for (var i = 0; i < data.length; i++) {
        data[i].color = data[i][attr]== null ? "#999" : colorHash.hex(data[i][attr]);
        data[i].textColor = data[i][attr]== null ? "#999" : darkColorHash.hex(data[i][attr]);
      }
    }
  }
}

function setRadius(elem, attr, min = 1, max = 9){
  data = eval(elem);
  if (attr == 'none') {
    for (var i = 0; i < data.length; i++) {
      data[i].cartR = min
    }
  }
  else{
    var minVal = Math.min.apply(Math, data.map(function(o) { return o[attr]; }));
    var maxVal = Math.max.apply(Math, data.map(function(o) { return o[attr]; }));
    var scale = d3.scaleLinear()
      .domain([Math.sqrt(minVal), Math.sqrt(maxVal)])
      .range([min, max]);
    for (var i = 0; i < data.length; i++) {
      data[i].cartR = data[i][attr] == null ? min : scale(Math.sqrt(data[i][attr]));
    }
  }
}

function plotCities(){
  var circles = plotCircles('cities', svg);
  
  circles.selectAll("circle")
    .attr("fill-opacity", ".1")
    .attr("stroke-width", strokeWidth)
    .attr("stroke-opacity", "1");
  circles.selectAll("text")
    .attr("font-size", labelSize)
    .text(function(d){ return d.name});
  
}


function plotStrongholds(){
  setColor('strongholds', 'tribe');
  
  var strongholdNode = svg.selectAll(".strongholds");
  var circles = plotCircles('strongholds', strongholdNode);
  
  circles.selectAll("circle")
    .attr("r", function(d) {return d.cartR})
    .attr("fill-opacity", "0")
    .attr("stroke", function(d) {return d.color})
    .attr("stroke-opacity", ".6")
    .attr("stroke-width", strokeWidth*4);
  circles.selectAll("text")
    .attr("font-size", labelSize)
    .text(function(d){ return d.name});
  
  
}

function plotCircles(elem, node){
  var circles = node.append("g")
    .attr("class", elem)
    .selectAll("circle")
    .data(eval(elem))
    .enter().append("g");
  
  circles.append("circle")
    .attr("r", 1)
    .attr("cx", function(d) { return d.cartX} )
    .attr("cy", function(d) { return d.cartY} )
    .attr("fill", "#FFF");
    
  circles.append("text")
    .attr("class", "label")
    .attr("x", function(d) { return 9999; })
    .attr("y", function(d) { return d.cartY+.7; })
    .attr("font-weight", "bold");
  
  return circles;
}

function redrawCities() {
  svg.selectAll('.cities')
    .data(cities);
  
  svg.selectAll('.cities')
    .selectAll("circle")
    .style("stroke", function(d) {return d.color})
    .attr("r", function(d) {return d.cartR});
  
  svg.selectAll('.cities')
    .selectAll("text")
    .style("fill", function(d) {return d.textColor});
}


function projectCoords(data){
  for (var i = 0; i < data.length; i++) {
    //odd y value = add 1 to x
    var cartX = xScale((data[i].y & 1) + (data[i].x << 1));
    var cartY = yScale(data[i].y);
    data[i].cartX = cartX;
    data[i].cartY = cartY;
  }
}

function setupMap(){
  var div = 8000,
    unitX = 8,
    unitY = 32;
  var len = (6800/unitX+1)*(6200/unitY+1);
  var width = (6800/unitX+1);
  // (map = []).length = len;
  // map.fill({});
  // map = Array(len).fill({value:500, owner:-1});
  map = []
  for (var i = 0; i < len; i++){
    var pt = {}
    pt.x = (i % width) * unitX;
    pt.y = Math.floor(i/width) * unitY;
    pt.width = xScale(unitX)-xScale(0);
    pt.height = yScale(unitY)-yScale(0);
    pt.value = 400;
    pt.owner = -1;
    pt.opacity = .4;
    map.push(pt);
  }
  
  
  for (var i = 0; i < cities.length; i++){
    var influence = Math.sqrt(cities[i].expense_value+1) / Math.sqrt(div),
      x = cities[i].x,
      y = cities[i].y,
      range = cities[i].expense_value/ div;
    
    var minX = Math.max(x - range * 1.5, 0),
      maxX = Math.min(x + range * 1.5, 6800),
      minY = Math.max(y - range * 3, 0),
      maxY = Math.min(y + range * 3, 6200);
    
    minX = Math.ceil(minX/unitX);
    maxX = Math.floor(maxX/unitX);
    minY = Math.ceil(minY/unitY);
    maxY = Math.floor(maxY/unitY);
    
    for (var tempX = minX; tempX <= maxX; tempX ++){
      for (var tempY = minY; tempY <= maxY; tempY ++){
        var distance = Math.abs(x/unitX - tempX)/2 + Math.abs(y/unitY - tempY);
        var value = distance * div / (influence*influence + 1);
        if (map[tempX+tempY*width].value > value){
          map[tempX+tempY*width].value = value;
          map[tempX+tempY*width].owner = i;
        }
      }
    }
  }
  projectCoords(map);
  var newMap = [];
  for (let pt of map){
    if (pt.owner >= 0){
      newMap.push(pt);
    }
  }
  map = newMap;
  plotMap();
  reMap();
}



function plotMap(){
  svg.selectAll(".map")
    .selectAll("rect")
    .data(map)
    .enter().append("rect")
    .attr("x", function(d) { return d.cartX - d.width;} )
    .attr("y", function(d) { return d.cartY - d.height/2;} )
    .attr("width", function(d) { return d.width*2})
    .attr("height", function(d) { return d.height})
    .on("click", function(d) { selectCity(d.owner)});
}

function reMap(){
  for (var i = 0; i < map.length; i++){
    map[i].color = cities[map[i].owner].color;
  }
  svg.selectAll('.map')
    .selectAll("rect")
    .data(map)
    .style("fill", function(d) {return d.color})
    .style("fill-opacity", function(d) {return d.opacity});
}
function selectCity(city){
  prevSel = sel;
  sel = city;
  
}
function resetSelection(){
  prevSel = sel;
  sel = -1;
}

function zoomed() {
  svg.attr("transform", d3.event.transform);
  zoom = d3.event.transform.k;
  var newStrokeWidth = strokeWidth/zoom;
  svg.selectAll("circle").attr("stroke-width", newStrokeWidth);
  svg.selectAll(".strongholds circle").attr("stroke-width", newStrokeWidth*4);
  svg.selectAll("path").attr("stroke-width", newStrokeWidth);
  
  svg.selectAll(".label")
    .attr("x", function (d) {
      if (zoom >= 4){
        return d.cartX+1.5;;
      }
      else{
        return 9999;
      }
    })
    .attr("font-size", labelSize/zoom)
}