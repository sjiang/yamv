var sel = -1;
var prevSel = -1;
var zoom = 1;

var svg = d3.select("#svg")
  .append("svg")
  .attr("preserveAspectRatio", "xMinYMin meet")
  .attr("viewBox", "0 0 "+width+" "+height)
  .classed("svg-content-responsive", true)
  .style("border","none") 
  .style("background-color", "none")
  .call(d3.zoom()
         .on("zoom", function () {zoomed();})
         .scaleExtent([1,5])
         .translateExtent([[0,0],[width,height]])
       )
  .append("g");

var bg = svg.append("rect")
  .attr("x", xScale(0))
  .attr("y", yScale(0))
  .attr("width", xScale(6800)-xScale(0))
  .attr("height", yScale(6200)-yScale(0))
  .style("fill", "#FFF")
  .style("stroke", "#888")
  .style("stroke-width", 1)
  .on("click", function () {
    resetSelection();
  });

svg.append("g")
    .attr("class", "map");

svg.append("g")
    .attr("class", "barbs");

svg.append("g")
    .attr("class", "strongholds");

var cities,
  barbs,
  strongholds;
var map;

d3.json("json/map.json", function(data) {
  cities = data["cities"];
  forests = data["forests"];
  tribes = data["tribes"];
  barbs = data["barbarian_tribes"];
  strongholds = data["strongholds"];
  players = data["players"];
  
  processData();
  
  setColor('cities', 'tribe');
  setRadius('cities', 'none');
  setRadius('strongholds', 'level');
  
  plotCities();
  
  plotStrongholds();
  
  redrawCities();
  
  setupMap();
  
  
  setupOptions();
});

