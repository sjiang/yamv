var cityColorOptions = ['tribe', 'player', 'expense_value', 'loot_stolen', 'attack_points', 'defense_point', 'none'];

var cityRadiusOptions = ['none', 'expense_value', 'loot_stolen', 'attack_points', 'defense_point'];

function setupOptions() {
  var options = document.getElementById("options");

  var cityColorSelect = document.createElement("select");
  cityColorSelect.setAttribute('id', 'cityColor');
  cityColorSelect.setAttribute('onchange', "cityColorOption(this)");

  options.appendChild(cityColorSelect);
  
  
  var cityRadiusSelect = document.createElement("select");
  cityRadiusSelect.setAttribute('id', 'cityRadius');
  cityRadiusSelect.setAttribute('onchange', "cityRadiusOption(this)");

  options.appendChild(cityRadiusSelect);

  for (var i = 0; i < cityColorOptions.length; i++) {
    var cityColorOption = document.createElement("option");
    cityColorOption.setAttribute('value', cityColorOptions[i]);
    cityColorOption.innerHTML = cityColorOptions[i];

    document.getElementById("cityColor").appendChild(cityColorOption);
  }

  for (var i = 0; i < cityRadiusOptions.length; i++) {
    var cityRadiusOption = document.createElement("option");
    cityRadiusOption.setAttribute('value', cityRadiusOptions[i]);
    cityRadiusOption.innerHTML = cityRadiusOptions[i];

    document.getElementById("cityRadius").appendChild(cityRadiusOption);
  }


}

function cityColorOption(selection){
  setColor('cities', selection.value);
  redrawCities();
  reMap();
}

function cityRadiusOption(selection){
  setRadius('cities', selection.value);
  redrawCities();
}